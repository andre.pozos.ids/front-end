function buscarMath(event) {
    event.preventDefault();

    const num = document.querySelector("#numMath").value;
    const loading = document.querySelector("#control-num-math");
    const resultadoMath = document.querySelector("#resultadoMath");

    mandarPeticion(`/${num}/math`, resultadoMath, loading);
}

function buscarTrivia(event) {
    event.preventDefault();

    const num = document.querySelector("#numTrivia").value;
    const loading = document.querySelector("#control-num-trivia");
    const resultadoTrivia = document.querySelector("#resultadoTrivia");
    mandarPeticion(`/${num}/trivia`, resultadoTrivia, loading);
}

function mandarPeticion(url, elemResultado, elemLoading) {
    var http = new XMLHttpRequest();

    http.open("GET", `http://numbersapi.com${url}`);
    http.send();
    elemLoading.classList.add("is-loading");
    http.addEventListener("load", function () {
        elemLoading.classList.remove("is-loading");
        elemResultado.textContent = this.response;
    });
    http.addEventListener("error", () => {
        elemLoading.classList.remove("is-loading");
    });
}
