/**
 * Este función resuelve una promesa y lanza 3 timeouts.
 * Los timeouts se lanzan uno tras otro y se acaban en el tiempo definido.
 */
async function conTimeout() {
    Promise.resolve(1)
        .then((result) => {
            const ms = 1000;
            console.log(`t${ms}`);
            setTimeout(() => {
                console.log(`A los ${ms}`);
            }, ms);
        })
        .then(() => {
            const ms = 500;
            console.log(`t${ms}`);
            setTimeout(() => {
                console.log(`A los ${ms}`);
            }, ms);
        })
        .then(() => {
            const ms = 2000;
            console.log(`t${ms}`);
            setTimeout(() => {
                console.log(`A los ${ms}`);
            }, ms);
        });
}

/**
 * Este función resuelve una promesa y procede a pausar la ejecución de las funciones cada
 * tantos milisegundos.
 */
async function conSleep() {
    Promise.resolve(1)
        .then(async () => {
            const ms = 1000;
            console.log(`Sleep ${ms}`);
            await sleep(ms);
        })
        .then(async () => {
            const ms = 500;
            console.log(`Sleep ${ms}`);
            await sleep(ms);
        })
        .then(async () => {
            const ms = 2000;
            console.log(`Sleep ${ms}`);
            await sleep(ms);
        });
}

/**
 * Esta función resuelve una promesa y bloquea la ejecución de la función
 * cada tantos segundos.
 * CUIDADO, bloquea el hilo principal.
 */
function conSleepSync() {
    Promise.resolve(1)
        .then(() => {
            const ms = 1000;
            console.log(`Sleep sync${ms}`);
            sleepSync(ms);
        })
        .then(() => {
            const ms = 500;
            console.log(`Sleep sync${ms}`);
            sleepSync(ms);
        })
        .then(() => {
            const ms = 2000;
            console.log(`Sleep sync${ms}`);
            sleepSync(ms);
        });
}

/**
 * Este función demuestra que los awaits son secuenciales,
 * bloqueando la ejecución de la función cada X milisegundos.
 */
async function conAsAw() {
    let ms = 1000;
    console.log(`Sleep async/await${ms}`);
    await sleep(ms);

    ms = 500;
    console.log(`Sleep async/await${ms}`);
    await sleep(ms);

    ms = 2000;
    console.log(`Sleep async/await${ms}`);
    await sleep(ms);
}

/**
 * Esta función bloquea la ejecución de la función en la que se llama.
 * Básicamente un mock de una operación que tarda mucho tiempo.
 * @param {Number} ms el número de milisegundos en el se detiene la ejecución.
 */
async function sleep(ms) {
    await new Promise((resolve) => setTimeout(resolve, ms));
}

/**
 * Esta función bloquea la ejecución de la función en la que se llama.
 * CUIDADO, también bloquea el hilo principal.
 * @param {Number} ms el número de milisegundos en el se detiene la ejecución.
 */
function sleepSync(ms) {
    var date = new Date();
    var curDate = null;
    do {
        curDate = new Date();
    } while (curDate - date < ms);
}
