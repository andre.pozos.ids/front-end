function f(x, y = 2, z = 7) {
    return x + y + z;
}

function fn1() {
    var animal = "kitty";
    var result = animal === "kitty" ? "cute" : "still nice";

    console.log(result); // output: cute
}

function fn2() {
    var animal = "kitty";
    var result = "";
    if (animal === "kitty") {
        result = "cute";
    } else {
        result = "still nice";
    }

    console.log(result); // output: cute
}

function fn3() {
    var a = 0;
    var str = "not a";
    var b = "";
    b = a === 0 ? ((a = 1), (str += " test")) : (a = 2);

    console.log(b); // output: not a test
}

function fn4() {
    var a = 1;
    a === 1 ? console.log("Hey, it is 1") : 0;
    console.log(a); // output: 1
}

function fn5() {
    // output: ReferenceError: a is not defined
    a === 1 ? console.log("Hey, it is 1") : console.log("Weird, what could it be?");
    if (a === 1) console.log("Hey, it is 1");
    else console.log("Weird, what could it be?");
}

function fn6() {
    var animal = "kitty";
    for (var i = 0; i < 5; i++) {
        // No es código JS válido
        //(animal === "kitty") ? break : console.log(i);
    }
}

function fn7() {
    var value = 1;
    switch (value) {
        case 1:
            console.log("I will always run");
            break;
        case 2:
            console.log("I will never run");
            break;
    }
    // output: I will always run
}

function fn8() {
    var animal = "Lion";
    switch (animal) {
        case "Dog":
            console.log("I will not run since animal !== 'Dog'");
            break;
        case "Cat":
            console.log("I will not run since animal !== 'Cat'");
            break;
        default:
            console.log("I will run since animal does not match any other case");
    }

    // output: I will run since animal does not match any other case
}

function fn9() {
    function john() {
        return "John";
    }

    function jacob() {
        return "Jacob";
    }
    // output: ReferenceError: name is not defined
    switch (name) {
        case john():
            console.log("I will run if name === 'John'");
            break;
        case "Ja" + "ne":
            console.log("I will run if name === 'Jane'");
            break;
        case john() + " " + jacob() + " Jingleheimer Schmidt":
            console.log("His name is equal to name too");
            break;
    }
}

function fn10() {
    var x = "c";
    switch (x) {
        case "a":
        case "b":
        case "c":
            console.log("Either a, b or c was selected");
            break;
        case "d":
            console.log("Only d was selected");
            break;
        default:
            console.log("No case was matched");
            break;
    }
    // output: Either a, b or c was selected
}

function fn11() {
    var x = 5 + 7;
    console.log(x); // output: 12

    x = 5 + "7";
    console.log(x); // output: 57

    x = "5" + 7;
    console.log(x); // output: 57

    x = 5 - 7;
    console.log(x); // output: -2

    x = 5 - "7";
    console.log(x); // output: -2

    x = "5" - 7;
    console.log(x); // output: -2

    x = 5 - "x";
    console.log(x); // output: NaN
}

function fn12() {
    var a = "hello" || "";
    console.log(a); // output: hello

    var b = "" || [];
    console.log(b); // output: []

    var c = "" || undefined;
    console.log(c); // output: undefined

    var d = 1 || 5;
    console.log(d); // output: 1

    var e = 0 || {};
    console.log(e); // output: {}

    var f = 0 || "" || 5;
    console.log(f); // output: 5

    var g = "" || "yay" || "boo";
    console.log(g); // output: "yay"
}

function fn13() {
    var a = "hello" && "";
    console.log(a); // output: ""

    var b = "" && [];
    console.log(b); // output: ""

    var c = undefined && 0;
    console.log(c); // output: undefined

    var d = 1 && 5;
    console.log(d); // output: 5

    var e = 0 && {};
    console.log(e); // output: 0

    var f = "hi" && [] && "done";
    console.log(f); // output: true

    var g = "bye" && undefined && "adios";
    console.log(g); // undefined
}

function fn14() {
    var foo = function (val) {
        return val || "default";
    };

    console.log(foo("burger")); // output: burger
    console.log(foo(100)); // output: 100
    console.log(foo([])); // output: []
    console.log(foo(0)); // output: default
    console.log(foo(undefined)); // output: default
}

function fn15() {
    // Reference error porque age, height, status, hasInvitation y suitable
    // no están definidas
    var isLegal = age >= 18;
    var tall = height >= 5.11;
    var isSuitable = isLegal && tall;
    var isRoyalty = status === "royalty";
    var specialCase = isRoyalty && hasInvitation;
    var canEnterOurBar = suitable || specialCase;
}

function fn16() {
    for (var i = 0; i < 3; i++) {
        if (i === 1) {
            continue;
        }
        console.log(i); // output: 0 y 2
    }
}

function fn17() {
    var i = 0;
    while (i < 3) {
        if (i === 1) {
            i = 2;
            continue;
        }

        console.log(i); // output: 0 y 2
        i++;
    }
}

function fn18() {
    for (var i = 0; i < 5; i++) {
        nextLoop2Iteration: for (var j = 0; j < 5; j++) {
            if (i == j) break nextLoop2Iteration;
            console.log(i, j);
            /* output:
            1 0
            2 0
            2 1
            3 0
            3 1
            3 2
            4 0
            4 1
            4 2
            4 3 
            */
        }
    }
}

function fn19() {
    function foo() {
        var a = "hello";

        function bar() {
            var b = "world";

            console.log(a);
            console.log(b);
        }

        console.log(a);
        console.log(b);
    }

    console.log(a); // ReferenceError: b is not defined
    console.log(b);
}

function fn20() {
    function foo() {
        const a = true;
    }

    function bar() {
        const a = false;
        console.log(a);
    }

    const a = false;
    a = false; // TypeError: Assignment to constant variable
    console.log(a);
}

function fn21() {
    var namedSum = function sum(a, b) {
        return a + b;
    };

    var anonSum = function (a, b) {
        return a + b;
    };

    console.log(namedSum(1, 3)); // output: 4
    console.log(anonSum(1, 3)); // output: 4
}

function fn22() {
    var a = [1, 2, 3, 8, 9, 10];
    console.log(a.splice(0, 3).concat([4, 5, 6, 7])); // output: [1, 2, 3, 4, 5, 6, 7]

    console.log(a.slice(3, 6)); // output: []

    var a = [1, 2, 3, 8, 9, 10];
    console.log(a.splice(0, 3, ...[4, 5, 6, 7])); // output: [1, 2, 3]
    console.log(a);
}

function fn23() {
    var array = ["a", "b", "c"];
    console.log(array.join("->")); // output: a->b->c
    console.log(array.join(".")); // output: a.b.c
    console.log("a.b.c".split(".")); // output: [a, b, c]
    console.log("5.4.3.2.1".split(".")); // output: [5, 4, 3, 2, 1]
}

function fn24() {
    var array = [5, 10, 15, 20, 25];

    console.log(Array.isArray(array)); // output: true
    console.log(array.includes(10)); // output: true
    console.log(array.includes(10, 2)); // output: false
    console.log(array.indexOf(25)); // output: 4
    console.log(array.lastIndexOf(10, 0)); // output: -1
}

function fn25() {
    var array = ["a", "b", "c", "d", "e", "f"];

    console.log(array.copyWithin(5, 0, 1)); // output: ["a", "b", "c", "d", "e", "e"]
    console.log(array.copyWithin(3, 0, 3)); // output: ["a", "b", "c", "a", "b", "c"]
    console.log(array.fill("Z", 0, 5)); // output: ["Z", "Z", "Z", "Z", "Z", "c"]
}

function fn26() {
    var array = ["Alberto", "Ana", "Mauricio", "Bernardo", "Zoe"];

    console.log(array.sort());
    // output: [ 'Alberto', 'Ana', 'Bernardo', 'Mauricio', 'Zoe' ]
    console.log(array.reverse());
    // output: [ 'Zoe', 'Mauricio', 'Bernardo', 'Ana', 'Alberto' ]
}

/*console.log(f(5, undefined)); // output: 14
fn1();
fn2();
fn3();
fn4();
//fn5();
fn6();
fn7();
fn8();
//fn9();
fn10();
fn11();
fn12();
fn13();
fn14();
//fn15();
fn16();
fn17();
fn18();
fn19();
fn20();
fn21();
fn22();
fn23();
fn24();
fn25();*/
fn26();
