const posBrowser = document.querySelector("#posBrowser");
const posPage = document.querySelector("#posPage");
const teclas = document.querySelector("#teclas");

function mostrarMovimiento(evento) {
    posBrowser.textContent = `${evento.screenX}, ${evento.screenY}`;
    posPage.textContent = `${evento.pageX}, ${evento.pageY}`;
}

function mostrarPulsaciones(event) {
    const texto = `Caracter: [${event.key}]`;
    teclas.appendChild(document.createTextNode(texto));
    teclas.appendChild(document.createElement("br"));
}
