function p1() {
    const clientes = ["c1", "c2", "c3", "c4", "c5"];
    const empleados = ["e1", "e2", "e3", "e4", "e5"];

    console.log(clientes.concat(empleados)); // Concat es la mejor opción
    console.log(clientes.join(",") + empleados.join(","));

    // Push también podría ser si le aplicamos el operador ... a empleados
    clientes.push(empleados);
    console.log(clientes);

    // Splice borra elementos, así que no es una opción
}

function p2() {
    var numbers = [5, 32, 43, 4];

    // filter() itera sobre el areglo, ejecuta la función con cada elemento como argumento,
    // y añade a un arreglo nuevo cuando la función devuelve un valor truthy.
    // En este caso, añade al nuevo arreglo aquellos números que sean impares.
    var resultado = numbers.filter(function (n) {
        return n % 2 !== 0;
    });

    console.log(resultado);
}

function p3() {
    var people = [
        {
            id: 1,
            name: "John",
            age: 28,
        },
        {
            id: 2,
            name: "Jane",
            age: 31,
        },
        { id: 3, name: "Peter", age: 55 },
    ];

    const personasMenoresDe35 = people.filter((persona) => persona.age <= 35);

    console.log(personasMenoresDe35);
}

function p4() {
    let people = [
        { name: "bob", id: 1 },
        { name: "john", id: 2 },
        { name: "alex", id: 3 },
        { name: "john", id: 3 },
    ];

    const ocurrencias = {};

    for (let { name } of people) {
        if (!ocurrencias[name]) {
            ocurrencias[name] = 1;
        } else {
            ++ocurrencias[name];
        }
    }

    console.log(ocurrencias);
}

function p5() {
    var myArray = [1, 2, 3, 4];
    console.log(Math.max(...myArray));
    console.log(Math.min(...myArray));
}

function p6() {
    var object = {
        key1: 10,
        key2: 3,
        key3: 40,
        key4: 20,
    };

    const valores = [];

    for (let valor of Object.values(object)) {
        valores.push(valor);
    }

    console.log(valores.sort((a, b) => a - b));
}

console.log("Problema 1: ");
p1();

console.log("Problema 2: ");
p2();

console.log("Problema 3: ");
p3();

console.log("Problema 4: ");
p4();

console.log("Problema 5: ");
p5();

console.log("Problema 6: ");
p6();
