var personArr = [
    {
        personId: 123,
        name: "Jhon",
        city: "Melbourne",
        phoneNo: "1234567890",
    },
    {
        personId: 124,
        name: "Amelia",
        city: "Sidney",
        phoneNo: "1234567890",
    },
    {
        personId: 125,
        name: "Emily",
        city: "Perth",
        phoneNo: "1234567890",
    },
    {
        personId: 126,
        name: "Abraham",
        city: "Perth",
        phoneNo: "1234567890",
    },
];

const tabla = document.createElement("table");
const thead = document.createElement("thead");

const primerElemeneto = personArr[0];
for (let clave in primerElemeneto) {
    const th = document.createElement("th");
    th.appendChild(document.createTextNode(clave));
    thead.appendChild(th);
}

const tbody = document.createElement("tbody");

for (let persona of personArr) {
    const tr = document.createElement("tr");

    for (let dato of Object.values(persona)) {
        const td = document.createElement("td");
        td.appendChild(document.createTextNode(dato));
        tr.appendChild(td);
    }

    tbody.appendChild(tr);
}
tabla.appendChild(thead);
tabla.appendChild(tbody);

document.querySelector("#p1").appendChild(tabla);

function mostrarInfo(element) {
    const id = `ID Elemento: ${element.id}`;
    const isoId = `ISO ID: ${element.dataset.id}`;
    const dialCode = `Dial code: ${element.dataset.dialCode}`;

    alert(`Elemento seleccionado:\n${id}\n${isoId}\n${dialCode}`);
}
