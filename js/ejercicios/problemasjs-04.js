function fn1() {
    var anObject = {
        foo: "bar",
        length: "interesting",
        "0": "zero!",
        "1": "one!",
    };

    var anArray = ["zero.", "one."];

    // Al usar la notación [] con los objetos, la clave se parsea a String
    console.log(anArray[0], anObject[0]);
    console.log(anArray[1], anObject[1]);

    // Ambos objetos tienen la misma propiedad en este caso.
    // Si no existiera la propiedad "length"
    console.log(anArray.length, anObject.length);

    // No existe la propiedad "foo" en el array (y así debería ser)
    console.log(anArray.foo, anObject.foo);

    // En JS, tanto los arreglos como los objetos se considean de tipo objetos
    console.log(typeof anArray === "object", typeof anObject === "object");
    console.log(anArray instanceof Object, anObject instanceof Object);

    // Sin embargo, los Arrays son objetos específicos de tipo Array
    console.log(anArray instanceof Array, anObject instanceof Array);
    console.log(Array.isArray(anArray), Array.isArray(anObject));
}

function fn2() {
    var obj = {
        a: "hello",
        b: "this is",
        c: "javascript!",
    };

    const array = Object.values(obj);
    console.log(array);
}

function fn3() {
    for (let i = 0; i < 100; i += 2) {
        console.log(i);
    }
}

function fn4() {
    let zero = 0;
    function multiply(x) {
        return x * 2;
    }

    function add(a = 1 + zero, b = a, c = b + a, d = multiply(c)) {
        console.log(a + b + c, d);
    }

    add(1); // 4 4
    add(3); // 12 12
    add(2, 7); // 18 4
    add(1, 2, 5); // 8 10
    add(1, 2, 5, 10); // 8 10
}

function fn5() {
    class MyClass {
        constructor() {
            this.names_ = [];
        }

        set name(value) {
            this.names_.push(value);
        }

        get name() {
            return this.names_[this.names_.length - 1];
        }
    }

    const myClass = new MyClass();
    myClass.name = "Joe";
    myClass.name = "Bob";

    console.log(myClass.name); // Bob. Se llama al getter predefinido
    console.log(myClass.names_); // [ 'Joe', 'Bob' ] Simplemente imprimimos la propiedad
}

function fn6() {
    const classInstance = new (class {
        get prop() {
            return 5;
        }
    })();

    classInstance.prop = 10;
    // Esto no se debe hacer en JS
    console.log(classInstance.prop); // 5
}

function fn7() {
    class Queue {
        constructor() {
            const list = [];
            this.enqueue = function (type) {
                list.push(type);
                return type;
            };

            this.dequeue = function () {
                return list.shift();
            };
        }
    }

    var q = new Queue();
    q.enqueue(9);
    q.enqueue(8);
    q.enqueue(7);

    console.log(q.dequeue()); // 9
    console.log(q.dequeue()); // 8
    console.log(q.dequeue()); // 7

    // No hay método toString() sobreescrito
    console.log(q); // Queue { enqueue: [Function], dequeue: [Function] }.

    // Muestra los métodos solamente porque no tiene propiedades
    console.log(Object.keys(q)); // [ 'enqueue', 'dequeue' ]
}

function fn8() {
    class Person {
        constructor(first, last) {
            this._firstname = first;
            this._lastname = last;
        }

        get firstname() {
            return this._firstname;
        }
        set firstname(value) {
            this._firstname = value;
        }

        get lastname() {
            return this._lastname;
        }
        set lastname(value) {
            this._lastname = value;
        }
    }
    let person = new Person("John", "Doe");

    console.log(person.firstname, person.lastname);
    person.firstname = "Foo";
    person.lastname = "Bar";
    console.log(person.firstname, person.lastname);
}

fn1();
fn2();
fn3();
fn4();
fn5();
fn6();
fn7();
fn8();

var deleteBtn = document.querySelectorAll("[data-deletepost]");
for (let btn of deleteBtn) {
    btn.addEventListener(
        "click",
        function () {
            var t = this;
            if (confirm("¿Borrar post?")) {
                t.parentElement.remove();
            }
        },
        false
    );
}

var segs = document.querySelector("#segundos");
var mins = document.querySelector("#minutos");
var hrs = document.querySelector("#horas");
var segundos = 0;
var minutos = 0;
var horas = 0;
setInterval(() => {
    ++segundos;
    if (segundos >= 60) {
        ++minutos;
        segundos = 0;
    }
    if (minutos >= 60) {
        ++horas;
        minutos = 0;
    }

    segs.textContent = `${segundos < 10 ? 0 : ""}${segundos} seg`;
    mins.textContent = `${minutos < 10 ? 0 : ""}${minutos} min`;
    hrs.textContent = `${horas < 10 ? 0 : ""}${horas} h`;
}, 1000);
