class Calculadora {
    constructor() {
        this.clear = true;
        this._pantalla = document.querySelector("#pantalla");
        this._pantalla.textContent = 0;

        this._operando1 = "";
        this.operando1 = "";

        this._operando2 = "";
        this.operando2 = "";

        this.operador = "";

        this._resultado = 0;
        this.resultado = 0;
    }

    get operando1() {
        return this._operando1;
    }
    set operando1(valor) {
        if (valor.length >= 2 && valor.startsWith("0")) {
            valor = valor.slice(1);
        }
        this._operando1 = valor;
        this._pantalla.textContent = valor;
    }

    get operando2() {
        return this._operando2;
    }
    set operando2(valor) {
        if (valor.length >= 2 && valor.startsWith("0")) {
            valor = valor.slice(1);
        }
        this._operando2 = valor;
        this._pantalla.textContent = valor;
    }

    get resultado() {
        return this._resultado;
    }
    set resultado(valor) {
        this._pantalla.textContent = valor;
        this._resultado = valor;
    }

    teclear(num) {
        if (this.clear) {
            this._pantalla.textContent = "";
            this.clear = false;
        }
        if (!this.operador) {
            this.operando1 += num.toString();
        } else {
            this.operando2 += num.toString();
        }
    }
    operar(op) {
        if (this.clear) {
            this.clear = false;
            this.operando1 = 0;
        }
        if (this.operando2) {
            this.obtenerResultado();
            this._operando1 = this.resultado;
        }
        this.operador = op;
    }

    monoOperar(monoOp) {
        switch (monoOp) {
            case "±":
                {
                    if (!this.operador) {
                        this.operando1 *= -1;
                    } else {
                        this.operando2 *= -1;
                    }
                }
                break;
            case "%":
                {
                    if (!this.operador) {
                        this.operando1 /= 100;
                    } else {
                        this.operando2 /= 100;
                    }
                }
                break;
            case ".":
                {
                    if (!this.operador) {
                        this.operando1 += ".";
                    } else {
                        this.operando2 += ".";
                    }
                }
                break;
            case "ac":
                {
                    this._operando1 = "";
                    this.operando1 = "";

                    this._operando2 = "";
                    this.operando2 = "";

                    this.operador = "";

                    this._resultado = 0;
                    this.resultado = 0;
                    this.clear = true;
                }
                break;
        }
    }

    obtenerResultado() {
        const op1 = parseFloat(this.operando1);
        const op2 = parseFloat(this.operando2);
        switch (this.operador) {
            case "+":
                this.resultado = op1 + op2;
                break;
            case "-":
                this.resultado = op1 - op2;
                break;
            case "x":
                this.resultado = op1 * op2;
                break;
            case "/":
                if (op2 === 0) {
                    alert("No se puede dividir entre 0");
                }
                this.resultado = op1 / op2;
                this.monoOperar("ac");
                break;
        }
        this._operando1 = this.resultado;
        this._operando2 = "";
        this.operador = "";
    }
}

const calc = new Calculadora();
