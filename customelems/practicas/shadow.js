class MiSaludo extends HTMLElement {
    constructor() {
        super();
        const tpl = document.querySelector("template");
        const tplInst = tpl.content.cloneNode(true);

        this.attachShadow({
            mode: "open",
        });

        this.shadowRoot.appendChild(tplInst);
    }
}

customElements.define("mi-saludo", MiSaludo);
