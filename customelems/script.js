class MiBoton extends HTMLElement {
    constructor() {
        super();
        this.addEventListener("click", () => {
            alert("hola");
        });
    }
}

customElements.define("mi-boton", MiBoton);
customElements.define(
    "mi-boton-2",
    class extends HTMLElement {
        constructor() {
            super();
            this.addEventListener("click", () => {
                alert("hola 2");
            });
        }
    }
);

class MiBotonExtendido extends HTMLButtonElement {
    constructor() {
        super();
        this.addEventListener("click", () => {
            console.log(`Evento click ${this.innerHTML}`);
            alert("Boton click");
        });
    }

    static get ceName() {
        return "mi-boton-extendido";
    }

    get is() {
        return this.getAttribute("is");
    }

    set is(value) {
        this.setAttribute("is", value || this.ceName);
    }
}

customElements.define("mi-boton-extendido", MiBotonExtendido, {
    extends: "button",
});

const miBotonExtendido2 = document.createElement("button");

miBotonExtendido2.setAttribute("is", MiBotonExtendido.ceName);

miBotonExtendido2.textContent = "Soy un botón extendido";
document.body.appendChild(miBotonExtendido2);

const miBotonExtendido3 = document.createElement("button");

miBotonExtendido3.setAttribute("is", MiBotonExtendido.ceName);

miBotonExtendido3.textContent = "Hola btn 3";
document.querySelector("#container").appendChild(miBotonExtendido3);
