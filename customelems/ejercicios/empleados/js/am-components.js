class AMTabla extends HTMLElement {
    constructor() {
        super();
        const thead = document.createElement("thead");
        const tr = document.createElement("tr");

        const headers = this.crearHeader();
        tr.append(...headers);
        thead.appendChild(tr);
        this.appendChild(thead);
        this.appendChild(document.createElement("tbody"));
    }

    crearHeader() {
        const headersTexto = ["ID", "Nombre", "Apellido", "Salario", "Edad"];
        const headersElem = headersTexto.map((header) => {
            const th = document.createElement("th");
            th.textContent = header;

            return th;
        });

        return headersElem;
    }
}

class AMInput extends HTMLInputElement {
    constructor() {
        super();
        this.setAttribute("type", "search");
        //this.setAttribute("placeholder", "Buscar...");
        this.addEventListener("input", this.filtrar);
    }

    filtrar() {
        const empleados = document.querySelector("am-tabla tbody").childNodes;
        if (!this.value) {
            empleados.forEach((empleado) => {
                empleado.style.display = "table-row";
            });
        }
        const busqueda = new RegExp(this.value, "i");
        empleados.forEach((empleado) => {
            const nombre = empleado.children[1].textContent;
            const apellido = empleado.children[2].textContent;
            if (!busqueda.test(nombre) && !busqueda.test(apellido)) {
                empleado.style.display = "none";
            } else {
                empleado.style.display = "table-row";
            }
        });
    }
}

export { AMInput, AMTabla };
