import { AMInput, AMTabla } from "./am-components.js";

async function obtenerDatos() {
    const datos = await fetch("http://dummy.restapiexample.com/api/v1/employees");
    const { data } = await datos.json();
    return data;
}

function mostrarEmpleados(empleados) {
    const tbody = document.querySelector("am-tabla tbody");
    const empleadoTemplate = document.querySelector("template");

    for (let empleado of empleados) {
        const { id, employee_name, employee_salary, employee_age } = empleado;
        const [nombre, apellido] = employee_name.split(" ");
        const fila = empleadoTemplate.content.cloneNode(true);

        fila.querySelector(".nEmpleado").textContent = id;
        fila.querySelector(".nombre").textContent = nombre;
        fila.querySelector(".apellidos").textContent = apellido;
        fila.querySelector(".salario").textContent = employee_salary;
        fila.querySelector(".edad").textContent = employee_age;

        const tr = document.createElement("tr");
        tr.appendChild(fila);
        tbody.appendChild(tr);
    }
}

customElements.define("am-tabla", AMTabla);
customElements.define("am-input", AMInput, {
    extends: "input",
});

(async () => {
    const s = await obtenerDatos();
    mostrarEmpleados(s);
})();
