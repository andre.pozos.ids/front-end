import { TodoApp } from "./ToDoApp.js";
import { YInput } from "./YInput.js";
import { YPendientes } from "./YPendientes.js";

window.customElements.define("to-do-app", TodoApp);
window.customElements.define("y-input", YInput);
window.customElements.define("y-pendientes", YPendientes);
