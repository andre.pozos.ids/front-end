const template = document.createElement("template");
template.innerHTML = `
<link rel="stylesheet" href="bulma.min.css" />

<div class="container">
    <h1 class="title has-text-centered">Pendientes</h1>
    
    <div class="columns">
        <div class="column is-8 is-offset-2">
            <y-input></y-input>
            <y-pendientes></y-pendientes>
        </div>
    </div>
</div>
`;

class TodoApp extends HTMLElement {
    constructor() {
        super();
        this.attachShadow({ mode: "open" });
        this.shadowRoot.appendChild(template.content.cloneNode(true));

        this.shadowRoot.addEventListener("pendiente", (e) => {
            this.shadowRoot.querySelector("y-pendientes").nuevoPendiente = e.detail;
        });
    }
}

export { TodoApp };
