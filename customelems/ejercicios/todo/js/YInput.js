const template = document.createElement("template");
template.innerHTML = `
<link rel="stylesheet" href="bulma.min.css" />

<form class="field has-addons">
    <p class="control is-expanded">
        <input class="input is-rounded" type="text" placeholder="Nuevo pendiente..." />
    </p>
    <p class="control">
        <button class="button is-primary is-rounded" type="submit">Añadir</button>
    </p>
</form>
`;

class YInput extends HTMLElement {
    constructor() {
        super();
        this.attachShadow({ mode: "open" });
        this.shadowRoot.appendChild(template.content.cloneNode(true));

        this._form = this.shadowRoot.querySelector("form");
        this._form.addEventListener("submit", this.añadirPendiente.bind(this));
    }

    añadirPendiente(e) {
        e.preventDefault();
        const pendiente = this.shadowRoot.querySelector("input").value;
        this.shadowRoot.querySelector("input").value = null;
        this._form.dispatchEvent(
            new CustomEvent("pendiente", {
                bubbles: true,
                detail: pendiente,
                composed: true,
            })
        );
    }
}

export { YInput };
