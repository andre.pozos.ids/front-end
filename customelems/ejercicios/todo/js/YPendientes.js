const template = document.createElement("template");
template.innerHTML = `
<link rel="stylesheet" href="bulma.min.css" />
<link rel="stylesheet" href="estilos.css" />

<ul id="lista">
    <p class="has-text-centered" id="ninguno">No tienes ningún pendiente</p>
</ul>
`;

class YPendientes extends HTMLElement {
    constructor() {
        super();
        this.attachShadow({ mode: "open" });
        this.shadowRoot.appendChild(template.content.cloneNode(true));

        this._ninguno = this.shadowRoot.querySelector("#ninguno");
        this._lista = this.shadowRoot.querySelector("#lista");

        this.pendientes = [];
    }

    set nuevoPendiente(value) {
        this.pendientes.push(value);
        this.actualizarPendientes();
    }

    actualizarPendientes() {
        if (this.pendientes.length > 0) {
            this._ninguno.style.display = "none";

            const ultimoPendiente = this.pendientes.slice(-1)[0];
            const li = document.createElement("li");
            const checkBox = document.createElement("input");
            checkBox.type = "checkbox";
            checkBox.addEventListener("click", this.formatearPendiente);

            li.appendChild(checkBox);
            li.appendChild(document.createTextNode(ultimoPendiente));
            this._lista.appendChild(li);
        }
    }

    formatearPendiente() {
        if (this.checked) {
            this.parentElement.style.textDecoration = "line-through";
        } else {
            this.parentElement.style.textDecoration = "none";
        }
    }
}

export { YPendientes };
