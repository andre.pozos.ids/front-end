class MiMensaje extends HTMLElement {
    constructor() {
        super();
        this.addEventListener("click", () => {
            alert("Click en mensaje");
        });
        console.log("Constructor llamado");
    }

    static get observedAttributes() {
        return ["msj", "casi-visible"];
    }

    connectedCallback() {
        console.log("Insertado en el documento");
    }

    disconnectedCallback() {
        console.log("Elemento eliminado");
    }

    adoptedCallback() {
        console.log("Elemento adoptado por otro elemento");
    }

    attributeChangedCallback(attr, oldVal, newVal) {
        console.log("Cambia un atributo");
        if (attr === "msj") {
            this.pintarMensaje(newVal);
        }

        if (attr === "casi-visible") {
            this.setCasiVisible();
        }
    }

    pintarMensaje(newVal) {
        this.innerHTML = newVal;
    }

    get msj() {
        return this.getAttribute("msj");
    }

    set msj(value) {
        this.setAttribute("msj", value);
    }

    get casiVisible() {
        return this.hasAttribute("casi-visible");
    }

    set casiVisible(value) {
        if (value) {
            this.setAttribute("casi-visible", true);
        } else {
            this.removeAttribute("casi-visible");
        }
    }

    setCasiVisible() {
        if (this.casiVisible) {
            this.style.opacity = 0.1;
        } else {
            this.style.opacity = 1;
        }
    }
}

customElements.define("mi-mensaje", MiMensaje);

const miMensaje = document.createElement("mi-mensaje");
miMensaje.msj = "Otro mensaje";
document.body.appendChild(miMensaje);

const tercerMensaje = new MiMensaje();
tercerMensaje.msj = "Tercer mensaje";
document.body.appendChild(tercerMensaje);
