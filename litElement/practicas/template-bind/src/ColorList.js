import { html, css, LitElement } from "lit-element";
import { ListElement } from "./ListElement.js";

window.customElements.define("list-element", ListElement);

export class ColorList extends LitElement {
  static get styles() {
    return css`
      :host {
        display: block;
        padding: 25px;
        color: var(--template-bind-text-color, #000);
      }
    `;
  }

  static get properties() {
    return {
      colors: { type: Array },
    };
  }

  constructor() {
    super();
    this.colors = ["Rojo", "Verde", "Azul", "Amarillo"];
  }

  render() {
    return html` <list-element .items="${this.colors}"></list-element>`;
  }
}
