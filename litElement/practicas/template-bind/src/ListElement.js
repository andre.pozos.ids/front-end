import { html, css, LitElement } from "lit-element";

export class ListElement extends LitElement {
  static get styles() {
    return css`
      :host {
        display: block;
        padding: 25px;
        color: var(--template-bind-text-color, #000);
      }
    `;
  }

  static get properties() {
    return {
      items: { type: Array },
    };
  }

  render() {
    return html` <ul>
      ${this.items.map((item) => html`<li>${item}</li>`)}
    </ul>`;
  }
}
