import { html, css, LitElement } from "lit-element";

export class TemplateBind extends LitElement {
  static get styles() {
    return css`
      :host {
        display: block;
        padding: 25px;
        color: var(--template-bind-text-color, #000);
      }
    `;
  }

  static get properties() {
    return {
      prop1: { type: String },
      prop2: { type: String },
      prop3: { type: String },
      prop4: { type: String },
      activo: { type: Boolean },
    };
  }

  constructor() {
    super();
    this.prop1 = "text binding";
    this.prop2 = "my div";
    this.prop3 = true;
    this.prop4 = "pie";
    this.activo = true;
  }

  __increment() {
    this.counter += 1;
  }
  clickHandler(e) {
    console.log(e.target);
  }
  doChange(e) {
    this.activo = e.target.checked;
  }

  render() {
    return html` <div>${this.prop1}</div>
      <div id="${this.prop2}">Attribute binding</div>
      <div>
        Boolean attribute binding
        <input type="text" ?disabled="${this.prop3}" />
      </div>
      <div>
        Property binding
        <input type="text" .value="${this.prop4}" />
      </div>
      <div>
        Event handler binding
        <button @click="${this.clickHandler}">Click</button>
      </div>
      <input
        type="checkbox"
        ?checked="${this.activo}"
        @change="${this.doChange}"
      />`;
  }
}
