import { html, render } from "lit-html";

const cadena = ">>>contenido dinámico<<<";

const templateHolder = (paramString) => html` <h2>Contenido estático + ${paramString}</h2>`;
const objectTemplateResult = templateHolder(cadena);

render(objectTemplateResult, document.querySelector("#container1"));
render("otro tetxti", document.querySelector("#container2"));
