import { MyConverter } from "./src/MyConverter.js";
import { ObservedAttribute } from "./src/ObservedAttribute.js";
import { MyAccessors } from "./src/MyAccessors";
import { CustomHasChanged } from "./src/CustomHasChanged.js";

window.customElements.define("my-converter", MyConverter);
customElements.define("observed-attribute", ObservedAttribute);
customElements.define("my-accessors", MyAccessors);
customElements.define("custom-has-changed", CustomHasChanged);
