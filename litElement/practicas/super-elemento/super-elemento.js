import { SuperElemento } from "./src/SuperElemento.js";
import { HerenciaElemento } from "./src/HerenciaElemento.js";
import { SharingStyles } from "./src/SharingStyles.js";
import { ShadowTreeStyle } from "./src/ShadowTreeStyle";
import { HostElement } from "./src/HostElement.js";
import { SlotsElement } from "./src/SlotsElement";
import { ConfigStyle } from "./src/ConfigStyle";
import { DynamicStyle } from "./src/DynamicStyle";

window.customElements.define("super-elemento", SuperElemento);
customElements.define("herencia-elemento", HerenciaElemento);
customElements.define("sharing-styles", SharingStyles);
customElements.define("shadow-tree-style", ShadowTreeStyle);
customElements.define("host-element", HostElement);
customElements.define("slots-element", SlotsElement);
customElements.define("config-style", ConfigStyle);
customElements.define("dynamic-style", DynamicStyle);
