import { LitElement, html, css } from "lit-element";
export class ConfigStyle extends LitElement {
  static get properties() {
    return {};
  }

  constructor() {
    super();
  }

  static get styles() {
    return css`
      :host {
        color: var(--theme-color, yellowgreen);
      }
    `;
  }

  render() {
    return html`<section>Lorem</section>`;
  }
}
