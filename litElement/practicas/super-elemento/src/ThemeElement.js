import { LitElement, html, css } from "lit-element";

class ThemeElement extends LitElement {
  static get styles() {
    return css`
      :host {
        display: block;
        color: var(--my-element-text-color, black);
        background-color: var(--my-element-background-color, white);
        font-family: var(--my-element-font-family, Roboto);
      }
      :host([hidden]) {
        display: none;
      }
    `;
  }

  static get properties() {
    return {};
  }

  constructor() {
    super();
  }

  render() {
    return html`
    <p>
    <div>Lorem ipsum</div>
    </p>`;
  }
}

customElements.define("theme-element", ThemeElement);
