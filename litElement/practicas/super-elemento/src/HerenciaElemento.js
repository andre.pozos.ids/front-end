import { LitElement, html, css } from "lit-element";
import { SuperElemento } from "./SuperElemento.js";

export class HerenciaElemento extends SuperElemento {
  static get styles() {
    return [
      super.styles,
      css`
        button {
          color: red;
        }
      `,
    ];
  }
}
