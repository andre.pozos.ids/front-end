import { LitElement, html, css } from "lit-element";

export class HostElement extends LitElement {
  static styles = css`
    :host {
      display: block;
      background-color: azure;
    }
    :host([hidden]) {
      display: none;
    }
    :host(.blue) {
      background-color: aliceblue;
      color: blue;
    }
    p {
      font-family: fantasy;
    }
  `;

  render() {
    return html` <p>Lorem de nuevo</p>`;
  }
}
