import { LitElement, html, css } from "lit-element";

import { classMap } from "lit-html/directives/class-map";
import { styleMap } from "lit-html/directives/style-map";

export class DynamicStyle extends LitElement {
  static get properties() {
    return {
      classes: { type: Object },
      styles: { type: Object },
    };
  }
  constructor() {
    super();
    this.classes = { mydiv: true, someclass: true };
    this.style = { color: "green", fontFamily: "Roboto" };
  }

  static get styles() {
    return css`
      .mydiv {
        background-color: blue;
      }
      .someclass {
        border: 1px solid red;
      }
    `;
  }

  cambiar() {
    this.classes = { someclass: true, mydiv: !this.classes.mydiv };
  }

  render() {
    return html` <div
        class="${classMap(this.classes)}"
        style="${styleMap(this.styles)}"
      >
        oda
      </div>
      <button @click="${this.cambiar}">Aber</button>`;
  }
}
