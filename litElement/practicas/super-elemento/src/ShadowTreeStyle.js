import { LitElement, html, css } from "lit-element";

export class ShadowTreeStyle extends LitElement {
  static styles = css`
    * {
      color: red;
    }
    p {
      font-family: "sans-serif";
    }
    .my-class {
      margin: 100px;
    }
    #main {
      padding: 30px;
    }
    h1 {
      font-size: 4em;
    }
  `;

  render() {
    return html` <p>Lorem</p>
      <p class="my-class">Párrafo 1</p>
      <p id="main">Párrafo 2</p>
      <h1>T i t u l o</h1>`;
  }
}
