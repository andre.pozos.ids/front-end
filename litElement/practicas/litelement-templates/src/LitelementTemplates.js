import { html, css, LitElement } from 'lit-element';

export class LitelementTemplates extends LitElement {
  static get styles() {
    return css`
      :host {
        display: block;
        padding: 25px;
        color: var(--litelement-templates-text-color, #000);
      }
    `;
  }

  static get properties() {
    return {
      message: { type: String },
      myString: { type: String },
      myArray: { type: Array },
      myBool: { type: Boolean },
    };
  }

  constructor() {
    super();
    this.message = 'Loading';
    this.myString = 'Hola mundo';
    this.myArray = ['un', 'arreglo', 'de', 'datos'];
    this.myBool = true;

    this.addEventListener('stuff-loaded', ({ detail }) => {
      this.message = detail;
    });
    this.loadStuff();
  }

  loadStuff() {
    this.dispatchEvent(
      new CustomEvent('stuff-loaded', {
        detail: 'loaded',
      })
    );
  }

  render() {
    return html` <p>${this.message}</p>
      <p>Loops and conditionals</p>
      <p>${this.myString}</p>
      <ul>
        <li>${this.myArray.map(elem => html`<li>${elem}</li>`)}</li>
      </ul>
      ${this.myBool
        ? html`<p>Render if true</p>`
        : html`<p>Render if false</p>`}`;
  }
}
