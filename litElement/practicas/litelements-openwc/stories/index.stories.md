```js script
import { html } from '@open-wc/demoing-storybook';
import '../litelements-openwc.js';

export default {
  title: 'LitelementsOpenwc',
  component: 'litelements-openwc',
  options: { selectedPanel: "storybookjs/knobs/panel" },
};
```

# LitelementsOpenwc

A component for...

## Features:

- a
- b
- ...

## How to use

### Installation

```bash
yarn add litelements-openwc
```

```js
import 'litelements-openwc/litelements-openwc.js';
```

```js preview-story
export const Simple = () => html`
  <litelements-openwc></litelements-openwc>
`;
```

## Variations

###### Custom Title

```js preview-story
export const CustomTitle = () => html`
  <litelements-openwc title="Hello World"></litelements-openwc>
`;
```
