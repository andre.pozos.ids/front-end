import { html, css, LitElement } from "lit-element";

export class MyTextInput extends LitElement {
  static get styles() {
    return css`
      :host {
        display: block;
        padding: 25px;
        color: var(--input-sample-text-color, #000);
      }
    `;
  }

  static get properties() {
    return {
      value: { type: String },
    };
  }

  constructor() {
    super();
    this.value = "";
  }

  inputChange(e) {
    this.value = e.target.value;
    this.dispatchEvent(
      new CustomEvent("change", {
        detail: this.value,
      })
    );
  }

  render() {
    return html` <p>
      <input type="text" .value="${this.value}" @input="${this.inputChange}" />
    </p>`;
  }
}
