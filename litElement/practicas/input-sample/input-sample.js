import { InputSample } from "./src/InputSample.js";
import { MyTextInput } from "./src/MyTextInput.js";

window.customElements.define("my-text-input", MyTextInput);
window.customElements.define("input-sample", InputSample);
