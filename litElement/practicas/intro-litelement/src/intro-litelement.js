import { LitElement, html } from "lit-element";

export class IntroLitElement extends LitElement {
    static get is() {
        return "intro-lit-element";
    }
    render() {
        return html`
            <p>Hola intro-litelement</p>
            <p>Ahora estamos en MI zona 😎</p>
        `;
    }
}

customElements.define(IntroLitElement.is, IntroLitElement);
