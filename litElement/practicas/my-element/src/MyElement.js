import { html, css, LitElement } from "lit-element";

export class MyElement extends LitElement {
  static get styles() {
    return css`
      :host {
        display: block;
        padding: 25px;
        color: var(--my-element-text-color, #000);
      }
    `;
  }

  static get properties() {
    return {
      prop1: { type: String, reflect: true },
      prop2: { type: Number, reflect: true },
      prop3: { type: Boolean, reflect: true },
      prop4: { type: Array, reflect: true },
      prop5: { type: Object, reflect: true },
    };
  }

  constructor() {
    super();
    this.prop1 = "Hey there";
    this.prop2 = 5;
    this.prop3 = true;
    this.prop4 = [];
    this.prop5 = {};
  }

  changeAttributes() {
    const rand = Math.floor(Math.random() * 10);
    const myBool = this.getAttribute("prop3");

    this.setAttribute("prop1", rand.toString());
    this.setAttribute("prop2", rand.toString());
    this.setAttribute("prop3", myBool ? "" : null);
    this.setAttribute("prop4", JSON.stringify([...this.prop4, rand]));
    this.setAttribute("prop5", JSON.stringify({ ...this.prop5, rand }));

    this.requestUpdate();
  }

  changeProperties() {
    const rand = Math.floor(Math.random() * 10);
    const myBool = this.getAttribute("prop3");

    this.prop1 = rand.toString();
    this.prop2 = rand;
    this.prop3 = !myBool;
    this.prop4 = [...this.prop4, rand];
    this.prop5 = { ...this.prop5, rand };
  }

  attributeChangedCallback(name, oldVal, newVal) {
    console.log(`attr changed: ${name}: ${newVal}`);
    super.attributeChangedCallback(name, oldVal, newVal);
  }

  updated(changedProperties) {
    changedProperties.forEach((old, prop) => {
      console.log(`prop changed: ${prop}: ${old}`);
    });
  }

  render() {
    return html`<div>
      <p>prop1: ${this.prop1}</p>
      <p>prop2: ${this.prop2}</p>
      <p>prop3: ${this.prop3}</p>
      <p>
        prop4:
        ${this.prop4.map(
          (item, index) => html`<span>[${index}]:${item}</span>`
        )}
      </p>
      <p>
        prop5:
        ${Object.entries(this.prop5).map(
          ([key, value]) => html`<span>${key}:${value}</span>`
        )}
      </p>
      <button @click="${this.changeAttributes}">Atributos</button>
      <button @click="${this.changeProperties}">Propiedades</button>
    </div> `;
  }
}
