import { html, css, LitElement } from 'lit-element';

export class EContactList extends LitElement {
  static get styles() {
    return css`
    :host {
        display: block;
        padding: 25px;
        color: var(--e-contact-text-color, #000);
    `;
  }

  static get properties() {
    return {
      contactos: { type: Array },
    };
  }

  constructor() {
    super();
    this.contactos = [
      { nombre: 'andy', email: 'a@a.com' },
      { nombre: 'b', email: 'b@b.com' },
      { nombre: 'c', email: 'c@c.com' },
    ];
  }

  render() {
    return html`
      <div>
        ${this.contactos.map(
          contacto =>
            html`<e-contact
              nombre="${contacto.nombre}"
              email="${contacto.email}"
            ></e-contact>`
        )}
      </div>
    `;
  }
}
