import { LitElement, html, css } from 'lit-element';
import { classMap } from 'lit-html/directives/class-map';
import { styleMap } from 'lit-html/directives/style-map';

export class ListaDeseosApp extends LitElement {
  static get styles() {
    return css`
      :host {
        display: block;
      }
      ul {
        list-style-type: none;
        padding: 0px;
      }
      .dosMinutos {
        color: green;
      }
      .cincoMinutos {
        color: #ffff00;
        background-color: black;
      }
      .ochoMinutos {
        color: red;
      }
    `;
  }

  static get is() {
    return 'lista-deseos-app';
  }

  static get properties() {
    return {
      listaDeseos: { type: Array },
      intervalo: { type: Object },
    };
  }

  constructor() {
    super();
    this.listaDeseos = [];

    const INTERVALO_REPETICION = 1000 * 60;
    this.intervalo = setInterval(() => {
      const DOS_MINUTOS = 1000 * 60 * 2;
      const CINCO_MINUTOS = 1000 * 60 * 5;
      const OCHO_MINUTOS = 1000 * 60 * 8;

      for (const deseo of this.listaDeseos) {
        const fechaActual = Date.now();
        const diferenciaFecha = fechaActual - deseo.fecha;
        if (diferenciaFecha > DOS_MINUTOS) {
          deseo.estado.dosMinutos = true;
        }
        if (diferenciaFecha > CINCO_MINUTOS) {
          deseo.estado.dosMinutos = false;
          deseo.estado.cincoMinutos = true;
        }
        if (diferenciaFecha > OCHO_MINUTOS) {
          deseo.estado.dosMinutos = false;
          deseo.estado.cincoMinutos = false;
          deseo.estado.ochoMinutos = true;
        }
      }
      this.requestUpdate();
    }, INTERVALO_REPETICION);
  }

  disconnectedCallback() {
    clearInterval(this.intervalo);
  }

  set nuevoDeseo(value) {
    if (this.listaDeseos.length > 3) {
      return alert('Sólo se pueden 4 deseos');
    }

    const nuevoDeseo = {
      deseo: value,
      cumplido: false,
      fecha: Date.now(),
      estado: {
        dosMinutos: false,
        cincoMinutos: false,
        ochoMinutos: false,
      },
    };
    this.listaDeseos.push(nuevoDeseo);

    return this.requestUpdate();
  }

  cambiarDeseo({ target }) {
    const indice = target.value;
    this.listaDeseos[indice].cumplido = target.checked;
  }

  eliminarDeseos() {
    this.listaDeseos = this.listaDeseos.filter(deseo => !deseo.cumplido);

    const inputs = this.shadowRoot.querySelectorAll('input');
    for (const input of inputs) {
      input.checked = false;
    }
  }

  render() {
    return html` <div>
      <ul>
        ${this.listaDeseos.map(
          (deseo, index) =>
            html`<li
              class="${classMap(deseo.estado)}"
              style="${styleMap(
                deseo.cumplido
                  ? { textDecoration: 'line-through' }
                  : { textDecoration: 'none' }
              )}"
            >
              <input
                type="checkbox"
                .value="${index}"
                .checked="${deseo.cumplido}"
                @click="${this.cambiarDeseo}"
              />${deseo.deseo}
            </li>`
        )}
      </ul>

      <button type="button" @click="${this.eliminarDeseos}">
        Eliminar deseos cumplidos
      </button>
    </div>`;
  }
}
