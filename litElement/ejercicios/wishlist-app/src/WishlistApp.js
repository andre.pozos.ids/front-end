import { html, css, LitElement } from 'lit-element';
import { NuevoDeseoApp } from './NuevoDeseoApp.js';
import { ListaDeseosApp } from './ListaDeseosApp.js';

window.customElements.define(NuevoDeseoApp.is, NuevoDeseoApp);
window.customElements.define(ListaDeseosApp.is, ListaDeseosApp);

export class WishlistApp extends LitElement {
  static get styles() {
    return css`
      :host {
        display: block;
        padding: 0px 20px;
        color: var(--wishlist-app-text-color, #000);
      }
    `;
  }

  static get is() {
    return 'wishlist-app';
  }

  static get properties() {
    return {};
  }

  constructor() {
    super();
    this.addEventListener('nuevo-deseo', ({ detail }) => {
      this.shadowRoot.querySelector('lista-deseos-app').nuevoDeseo = detail;
    });
  }

  render() {
    return html`
      <h2>Mi lista de deseos</h2>
      <nuevo-deseo-app></nuevo-deseo-app>
      <lista-deseos-app></lista-deseos-app>
    `;
  }
}
