import { html, css, LitElement } from 'lit-element';

export class NuevoDeseoApp extends LitElement {
  static get styles() {
    return css`
      :host {
        display: block;
      }
      fieldset {
        border: none;
        padding: 0;
        margin: 0;
      }
      legend {
        padding: none;
        margin: 10px 0px;
        font-weight: bold;
      }
      input {
        background-color: transparent;
        border: none;
        border-bottom: 1px dashed black;

        width: 100%;
        height: 5vh;
      }
      input::placeholder {
        padding: 5px;
      }
    `;
  }

  static get is() {
    return 'nuevo-deseo-app';
  }

  static get properties() {
    return {
      deseo: { type: String },
    };
  }

  setDeseo({ target }) {
    this.deseo = target.value;
  }

  constructor() {
    super();
    this.deseo = '';
  }

  añadirDeseo(e) {
    e.preventDefault();

    this.dispatchEvent(
      new CustomEvent('nuevo-deseo', {
        detail: this.deseo,
        bubbles: true,
        composed: true,
      })
    );

    this.deseo = '';
  }

  render() {
    return html`
      <form @submit="${this.añadirDeseo}">
        <fieldset>
          <legend>Nuevo deseo</legend>
          <input
            type="text"
            placeholder="Escribe aquí un deseo"
            @input="${this.setDeseo}"
            .value="${this.deseo}"
          />
        </fieldset>
      </form>
    `;
  }
}
