import { LitElement, html, css } from "lit-element";

export class BuscadorApp extends LitElement {
  static get styles() {
    return css``;
  }

  constructor() {
    super();
    this.bulma = html`<link rel="stylesheet" href="./css/bulma.min.css" />`;
  }

  static get is() {
    return "buscador-app";
  }
  static get properties() {
    return {
      listaEmpleados: {
        type: Array,
      },
    };
  }

  set empleados(value) {
    this.listaEmpleados = value;
  }

  filtrar({ target }) {
    const criterio = target.value;
    const busqueda = new RegExp(criterio, "i");

    let nuevaLista;
    if (criterio) {
      nuevaLista = this.listaEmpleados.filter(({ employee_name }) => {
        const nombreCompleto = employee_name.split(" ");
        const [nombre, apellido] = nombreCompleto;
        return busqueda.test(nombre) || busqueda.test(apellido);
      });
    } else {
      nuevaLista = this.listaEmpleados;
    }

    this.dispatchEvent(
      new CustomEvent("filtrados", {
        detail: nuevaLista,
        bubbles: true,
        composed: true,
      })
    );
  }

  render() {
    return html` ${this.bulma}
      <label for="filtro">Filtrar empleados:</label>
      <input
        type="search"
        id="filtro"
        class="input"
        placeholder="Nombre o apellido..."
        @input="${this.filtrar}"
      />`;
  }
}
