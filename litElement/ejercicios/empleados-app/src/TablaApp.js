import { LitElement, html, css } from "lit-element";

export class TablaApp extends LitElement {
  static get styles() {
    return css`
      :host {
        display: block;
      }
    `;
  }

  constructor() {
    super();
    this.bulma = html`<link rel="stylesheet" href="./css/bulma.min.css" />`;
    this.empleados = [];
    this.headers = ["ID", "Nombre", "Apellido", "Salario", "Edad"];
  }

  static get is() {
    return "tabla-app";
  }
  static get properties() {
    return {
      empleados: {
        type: Array,
      },
      headers: {
        type: Array,
      },
    };
  }
  nombre(empleado) {
    return empleado.split(" ")[0];
  }
  apellido(empleado) {
    return empleado.split(" ")[1];
  }
  mostrarEmpleados(empleados) {
    this.empleados = empleados;
  }

  render() {
    return html`
      ${this.bulma}
      <table class="table is-fullwidth">
        <thead>
          ${this.headers.map((header) => html`<th>${header}</th>`)}
        </thead>
        <tbody>
          ${this.empleados.map((empleado) => {
            return html` <tr>
              <td class="nEmpleado">${empleado.id}</td>
              <td class="nombre">${this.nombre(empleado.employee_name)}</td>
              <td class="apellidos">
                ${this.apellido(empleado.employee_name)}
              </td>
              <td class="salario">${empleado.employee_salary}</td>
              <td class="edad">${empleado.employee_age}</td>
            </tr>`;
          })}
        </tbody>
      </table>
    `;
  }
}
