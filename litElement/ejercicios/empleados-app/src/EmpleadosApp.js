import { html, css, LitElement } from "lit-element";

export class EmpleadosApp extends LitElement {
  static get styles() {
    return css``;
  }

  constructor() {
    super();
    this.addEventListener("filtrados", ({ detail }) => {
      this.shadowRoot.querySelector("tabla-app").mostrarEmpleados(detail);
    });
  }
  static get is() {
    return "empleados-app";
  }
  static get properties() {
    return {};
  }
  async updated() {
    const empleados = await this.obtenerDatos();

    this.shadowRoot.querySelector("buscador-app").empleados = empleados;
    this.shadowRoot.querySelector("tabla-app").mostrarEmpleados(empleados);
  }

  async obtenerDatos() {
    const datos = await fetch(
      "http://dummy.restapiexample.com/api/v1/employees"
    );
    const { data } = await datos.json();
    return data;
  }

  render() {
    return html`
      <buscador-app></buscador-app>
      <tabla-app></tabla-app>
    `;
  }
}
