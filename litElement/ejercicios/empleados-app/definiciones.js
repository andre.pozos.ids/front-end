import { EmpleadosApp } from "./src/EmpleadosApp.js";
import { BuscadorApp } from "./src/BuscadorApp.js";
import { TablaApp } from "./src/TablaApp.js";

window.customElements.define(EmpleadosApp.is, EmpleadosApp);
window.customElements.define(BuscadorApp.is, BuscadorApp);
window.customElements.define(TablaApp.is, TablaApp);
